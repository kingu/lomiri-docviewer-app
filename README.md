ReadMe - Lomiri Doc Viewer App
==============================
Lomiri Doc Viewer App is the official document viewer app for Ubuntu Touch. We follow an open
source model where the code is available to anyone to branch and hack on. The
lomiri doc viewer app follows a test driven development (TDD) where tests are
written in parallel to feature implementation to help spot regressions easier.

Usage
=====
lomiri-docviewer-app [filename]

filename : The path where the file you want to open is located.

Dependencies
============
**DEPENDENCIES ARE NEEDED TO BE INSTALLED TO BUILD AND RUN THE APP**.

A complete list of dependencies for the project can be found in lomiri-docviewer-app/debian/control

The following essential packages are also required to develop this app:
* [ubuntu-sdk](http://developer.ubuntu.com/start)
* intltool   - run  `sudo apt-get install intltool`

Useful Links
============
Here are some useful links with regards to the Doc Viewer App development.

* [Home Page](https://gitlab.com/ubports/development/apps/docviewer-app)
* [Doc Viewer App Wiki](https://wiki.ubuntu.com/Touch/CoreApps/DocViewer)
* [Project page](https://gitlab.com/ubports/development/apps/docviewer-app)
